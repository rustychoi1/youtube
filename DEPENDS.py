import json
import sys

def reverse_map(mapping):
    rev_map = {}
    for key, values in mapping.items():
        key = key.replace('-', '_')
        for value in values:
            value = value.replace('-', '_')
            if value not in rev_map:
                rev_map[value] = []

            rev_map[value].append(key)

    return rev_map

def main(json_file):
    with open(json_file, 'r') as f:
        dependency = json.load(f)

    rev_map = reverse_map(dependency)

    vertices = rev_map.keys() | sum(rev_map.values(), [])

    print('digraph {')
    print('    rankdir=TB;')
    for v in vertices:
        print('    %s [fontname="Inconsolata"]' % v)

    print()
    for key, recvs in rev_map.items():
        print('    %s -> { %s };' % (key, ' '.join(recvs)))
    print('}')

if __name__ == '__main__':
    main(sys.argv[1])
