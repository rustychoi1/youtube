#!/usr/bin/env bash

declare -r ROOT=$(git rev-parse --show-toplevel)

declare -r DOT_FILE="$ROOT/DEPENDS.dot"

python3 "$ROOT/DEPENDS.py" "$ROOT/DEPENDS.json" > "$DOT_FILE"

dot -Tsvg -O "$DOT_FILE"
