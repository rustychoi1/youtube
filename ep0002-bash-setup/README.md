# Bash & Vim Setup

```bash
# check if you are currently running bash
echo $0

# if not, run this to start running bash
bash --login

cat ~/Downloads/ep0002-bash-setup_.bash_profile >> ~/.bash_profile
```
